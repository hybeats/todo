import { Component, OnInit } from '@angular/core';
import { FilterPipe } from '../filter.pipe';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  groups: string[] = ['Work', 'Home'];
  tasks: any[];
  taskText: string;
  queryText: string;
  selectedGroup: string;

  constructor() {}

  ngOnInit() {
    if (typeof window !== "undefined") {
      this.tasks = this.getFromStorage('todoData');
    }
  }
  ngAfterViewChecked() {
    this.setIntoStorage('todoData', this.tasks);
  }
  addTask() {
    if (this.taskText != '') {
      this.tasks.push({
        id: this.tasks.length + 1,
        groupId: (this.selectedGroup) ? this.selectedGroup : 'Unassigned',
        text: this.taskText,
        isCompleted: false
      });
      this.taskText = '';
    }
  }
  completeTask(i) {
    if(this.tasks[i].isCompleted != true) {
      let currentTask = this.tasks[i];
      this.tasks[i].isCompleted = true;
      this.tasks.splice(i, 1);
      this.tasks.push(currentTask);
    }
  }
  getFromStorage(key) {
    if (localStorage.getItem(key)) {
      return JSON.parse(localStorage.getItem(key));
    } else {
      return [];
    }
  }
  setIntoStorage(key, data) {
    let tempObj = JSON.stringify(data);
    localStorage.setItem(key, tempObj);
  }
  deleteStorageKey(key) {
    localStorage.removeItem(key);
  }
  clearData() {
    this.deleteStorageKey('todoData');
    this.tasks = [];
  }

}
